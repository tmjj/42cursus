/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 22:18:04 by cananwat          #+#    #+#             */
/*   Updated: 2022/03/03 19:46:21 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_int_len(int n)
{
	int	i;

	i = 1;
	if (n < 0)
	{
		i++;
		n = -n;
	}
	if (n == -2147483648)
	{
		i++;
		n = 147483648;
	}
	while (n / 10 > 0)
	{
		i++;
		n = n / 10;
	}
	return (i);
}

char	*ft_itoa(int n)
{
	char	*str;
	int		len;

	len = ft_int_len(n);
	str = (char *)malloc(sizeof(char) * (len + 1));
	if (n == -2147483648)
	{
		str[1] = '2';
		n = -147483648;
	}
	if (n < 0)
	{
		str[0] = '-';
		n = -n;
	}
	if (n == 0)
		str[0] = '0';
	str[len--] = '\0';
	while (n / 10 > 0)
	{
		str[len--] = (n % 10) + '0';
		n = n / 10;
	}
	str[len--] = n + '0';
	return (str);
}
