/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/26 11:17:10 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/26 13:45:27 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	input_len;
	size_t	i;

	input_len = ft_strlen(s);
	if (start > input_len)
	{
		str = (char *)malloc(sizeof(*s) * 1);
		str[0] = '\0';
		return (str);
	}
	if (len > (input_len - start))
		str = (char *)malloc(sizeof(*s) * (input_len - start + 1));
	else
		str = (char *)malloc(sizeof(*s) * len + 1);
	if (!str)
		return (NULL);
	i = 0;
	while (s[start + i] && i < len)
	{
		str[i] = s[start + i];
		i++;
	}
	str[i] = '\0';
	return (str);
}
