/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/23 21:49:54 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/24 11:23:15 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			i;
	unsigned char	*src;
	unsigned char	*dst;

	i = 0;
	src = (unsigned char *) s1;
	dst = (unsigned char *) s2;
	if (!src || !dst || n == 0)
		return (0);
	while (i < n && src[i] == dst[i])
		i++;
	if (i == n)
		return (0);
	return (*(src + i) - *(dst + i));
}
