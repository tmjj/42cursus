/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/19 11:41:29 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/19 21:45:22 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	if (!dst || !src)
		return (NULL);
	if (dst > src)
	{
		while (n-- > 0)
		{
			*(unsigned char *)(dst + n) = *(unsigned char *)(src + n);
		}
	}
	else
	{
		ft_memcpy(dst, src, n);
	}
	return (dst);
}
