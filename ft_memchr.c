/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/23 21:51:15 by cananwat          #+#    #+#             */
/*   Updated: 2022/02/23 22:16:08 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *str, int c, size_t n)
{
	size_t			i;
	unsigned char	*tmp;

	i = 0;
	tmp = (unsigned char *) str;
	if (!tmp)
		return (NULL);
	while (i < n)
	{
		if (tmp[i] == (unsigned char)c)
			return ((unsigned char *)(tmp + i));
		i++;
	}
	return (NULL);
}
