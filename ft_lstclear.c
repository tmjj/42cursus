/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/06 16:38:27 by cananwat          #+#    #+#             */
/*   Updated: 2022/03/06 18:40:31 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*tmp;
	t_list	*nx;

	if (!lst)
		return ;
	tmp = (*lst);
	while (tmp)
	{
		nx = tmp->next;
		ft_lstdelone(tmp, (del));
		tmp = nx;
	}
	*lst = NULL;
}
